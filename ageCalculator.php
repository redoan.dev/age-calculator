<?php
    date_default_timezone_set('Asia/Dhaka');
    // $dob=false;
    // $aad=false;
if(!isset($_POST['born']) && !isset($_POST['data'])){
    $date= date('Y-m-d');
    $_POST['data']=$date;
    $_POST['born']='1995-03-31';
    $d1=new DateTime($_POST['born']);
    $d2=new DateTime($date);
    $result=date_diff($d1,$d2);
}
if(isset($_POST['born']) && isset($_POST['data'])){
    $d=new DateTime($_POST['data']);
    $date= date_format($d,'Y-m-d');
    $d1=new DateTime($_POST['born']);
    $d2=new DateTime($date);
    $result=date_diff($d1,$d2);
    }
    //date_format($result,"%y Years %m Months %d Days");
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Age Calculator</title>
        <link rel="stylesheet" href="assests/css.css">
        <link rel="stylesheet" href="assests/normalize.css">
        <link rel="stylesheet" href="assests/milligram.min.css"> 
        <style>
            body{
                margin-top: 30px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="column column-60 column-offset-20">
                    <h1>Age Calculator</h1>
                    <p>Use this application to find your age</p>
                </div>
            </div>
            <div class="row">
                <div class="column column-60 column-offset-20">
                    <form method="POST" action="ageCalculator.php">
                        <label for="born">Date of Birth</label>
                        
                        <input type="date" name="born" id="born" value='<?php echo $_POST['born'];?>'>
                     
                        <label for="data">Age at Date</label>
                        
                        <input type="date" name="data" id="data" value='<?php echo $_POST['data'];?>'>
                     
                        <button type="result">Calculate Age</button>
                        <input type="text" name="result" id="result" value='<?php  echo $result->format('%y Years %m Months %d Days');?>'>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>